#include <assert.h>
#include <errno.h>
#include <iconv.h>
#include <inttypes.h>
#include <stdbool.h>
#include <string.h>
#include <strings.h>

#include <arpa/inet.h>

#include "edid.h"
#include "find_monitor.h"

// retrieve a potentially unaligned big-endian uint32_t
uint32_t unaligned_uint32(void *p) {
	uint8_t *pb = (uint8_t *)p;

	return pb[0] << 24 | pb[1] << 16 | pb[2] << 8 | pb[3];
}

// retrieve a potentially unaligned big-endian uint64_t
uint64_t unaligned_uint64(void *p) {
	uint8_t *pb = (uint8_t *)p;

	return (uint64_t)pb[0] << 56 | (uint64_t)pb[1] << 48 |
			(uint64_t)pb[2] << 40 | (uint64_t)pb[3] << 32 |
			(uint64_t)pb[4] << 24 | (uint64_t)pb[5] << 16 |
			(uint64_t)pb[6] << 8 | pb[7];
}

bool decode_string(iconv_t iconv_descriptor,
		char *input, size_t input_length,
		char *output, size_t output_size) {

	// iconv will move our pointers as it works so we need to use a copy of
	// output so we still have the original for later use
	char *sacrifical_output = output;
	size_t output_size_remaining = output_size;

	// convert from cp437
	if (iconv(iconv_descriptor,
				&input, &input_length,
				&sacrifical_output, &output_size_remaining) == (size_t)-1) {
		verbose("iconv: %s\n", strerror(errno));
	}

	// we need to null terminate regardless of failure or truncation so fake
	// the available size if needed
	if (output_size_remaining < 1) {
		output_size_remaining = 1;
	}

	// null terminate
	output[output_size - output_size_remaining] = '\0';

	// if the string is padded then truncate
	char *nl;
	if ((nl = index(output, '\n')) != NULL) {
		*nl = '\0';
	}

	return output;
}

bool parse_edid(uint8_t *edid, iconv_t iconv_descriptor,
		char *disp_name, size_t disp_name_size,
		char *disp_serial, size_t disp_serial_size,
		uint32_t *serial) {

	assert(disp_name_size > 0);
	assert(disp_serial_size > 0);

	// first check for the magic number
	if (unaligned_uint64(edid) != EDID_MAGIC) {
		// not an EDID record...?
		verbose("missing magic in EDID record");
		return false;
	}

	// get the serial number
	*serial = ntohl(unaligned_uint32(&edid[EDID_OFFSET_SERIAL]));

	// initialise our strings in case they are missing
	disp_serial[0] = '\0';
	disp_name[0] = '\0';

	// iterate through the 4 descriptors looking for strings of interest
	for (int di = 0; di < EDID_DESCRIPTOR_COUNT; di++) {
		// find the start of this descriptor
		size_t offset = EDID_OFFSET_DESCRIPTORS + di * EDID_DESCRIPTOR_SIZE;
		// find the type of this descriptor
		uint32_t d_type = unaligned_uint32(&edid[offset]);
		// see if it is an interesting one
		switch (d_type) {
			case EDID_DESCRIPTOR_TYPE_SERIAL:
				// display serial number
				if (! decode_string(iconv_descriptor,
							(char *)&edid[offset + EDID_DESCRIPTOR_TEXT_OFFSET],
							EDID_DESCRIPTOR_TEXT_SIZE,
							disp_serial, disp_serial_size)) {
					verbose("charset conversion for display serial failed ");
				}
				break;
			case EDID_DESCRIPTOR_TYPE_NAME:
				// display name
				if (! decode_string(iconv_descriptor,
							(char *)&edid[offset + EDID_DESCRIPTOR_TEXT_OFFSET],
							EDID_DESCRIPTOR_TEXT_SIZE,
							disp_name, disp_name_size)) {
					verbose("charset conversion for display name failed ");
				}
				break;
		}
	}

	return true;
}
