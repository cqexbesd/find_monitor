#include <err.h>
#include <fnmatch.h>
#include <iconv.h>
#include <inttypes.h>
#include <langinfo.h>
#include <locale.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <arpa/inet.h>

#include <X11/X.h>
#include <X11/Xatom.h>
#include <X11/Xlib.h>
#include <X11/extensions/Xrandr.h>

#include "edid.h"
#include "find_monitor.h"

static bool g_verbose;

static void usage(void) {
	fprintf(stderr, "usage: find_monitor [-v] -a\n"
			"       find_monitor [-v] [-n name] [-s display_serial] [-i serial] [-p port]\n"
			"\n"
			"Find which port a given monitor is connected to.\n"
			"\n"
			"-v	enable verbose mode (debugging)\n"
			"-a	list all ports and the EDID information available (default)\n"
			"\n"
			"Search criteria can be given with:\n"
			"-n	glob pattern to match against the EDID display name\n"
			"-s	glob pattern to match against the EDID display serial\n"
			"-i	the numeric serial from the EDID record\n"
			"-p	glob pattern to match against the port\n"
			"\n"
			"Search criteria are ANDed.\n");
}

// print to stderr if we are in verbose mode
__attribute__((__format__(__printf__, 1, 2)))
void verbose(const char *fmt, ...) {
	va_list ap;

	if (! g_verbose) {
		return;
	}

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
}

// setup iconv to convert from CP437 (chaset used in EDID strings) to the
// user's chosen charset
iconv_t init_iconv(void) {
	iconv_t iconv_descriptor;

	setlocale(LC_ALL, "");

	iconv_descriptor = iconv_open(nl_langinfo(CODESET), "CP437");
	if (iconv_descriptor == (iconv_t)-1) {
		err(1, "iconv_open");
	}

	return iconv_descriptor;
}

// init some X stuff and return success
bool init_x(Display **dpy, Atom *edid_atom, XRRScreenResources **screen) {

	// connect to X
	*dpy = XOpenDisplay(NULL);
	if (*dpy == NULL) {
		verbose("can't connect to the X server");
		return false;
	}

	// verify XRandR is present
	int major, minor;
	if (XRRQueryVersion(*dpy, &major, &minor) == 0) {
		verbose("No XRandR extension");
		goto init_x_fail0;
	}

	// verify it's new enough
	if (! (major > 1 || (major == 1 && minor > 3))) {
		verbose("XRandR extension too old (%d.%d)\n", major, minor);
		goto init_x_fail0;
	}

	// get the Atom for the edid property
	*edid_atom = XInternAtom(*dpy, RR_PROPERTY_RANDR_EDID, False);
	if (*edid_atom == None) {
		verbose("No Atom for \"%s\"", RR_PROPERTY_RANDR_EDID);
		goto init_x_fail0;
	}

	// get information on the "screen"
	*screen = XRRGetScreenResources(*dpy, DefaultRootWindow(*dpy));
	if (*screen == NULL) {
		verbose("XRRGetScreenResources");
		goto init_x_fail0;
	}

	// all well
	return true;

init_x_fail0:
	XCloseDisplay(*dpy);

	return 0;
}

// get the EDID for a given output
// don't forget to XFree edid when finished
bool edid_for_output(Display *dpy, RROutput output, Atom edid_atom,
		uint8_t **edid) {
	int i;
	Atom actual_type;
	int actual_format;
	unsigned long nitems;
	unsigned long bytes_after;

	if ((i = XRRGetOutputProperty(dpy, output, edid_atom,
				0, // offset
				EDID_SIZE, // length
				False, // _delete
				False, // pending
				XA_INTEGER, //AnyPropertyType, // req_type
				&actual_type, // 
				&actual_format,
				&nitems,
				&bytes_after,
				edid)) != Success) {
		verbose("XRRGetOutputProperty failed with code %d\n", i);
		return false;
	}

	// check we got what was expected
	// format == 8 meaning an array of 8 bit bytes
	if ((actual_type == XA_INTEGER) && (actual_format == 8)) {
		// looks good
		return true;
	}

	verbose("XRRGetOutputProperty gave the wrong type/format (%lu/%d)\n",
			actual_type, actual_format);

	// we need to clean up before returning a failure
	XFree(*edid);

	return false;
}

// check if this output matches our criteria for printing
bool handle_single_output(Display *dpy, XRRScreenResources *screen,
		const RROutput output, const Atom edid_atom,
		iconv_t iconv_descriptor,
		const bool list_all, const struct search_conditions *cond) {
	uint8_t *edid;
	bool found = false;

	// get details about the output
	XRROutputInfo *info = XRRGetOutputInfo(dpy, screen, output);
	if (info == NULL) {
		verbose("XRRGetOutputInfo returned NULL for output %lu",
				(unsigned long)(output));
		return false;
	}

	// check if there is anything plugged in
	if (info->connection == RR_Connected) {
		// there is so fetch the EDID
		if (edid_for_output(dpy, output, edid_atom, &edid)) {
			char disp_name[EDID_DESCRIPTOR_TEXT_SIZE + 1];
			char disp_serial[EDID_DESCRIPTOR_TEXT_SIZE + 1];
			uint32_t serial;

			// extract the things we want to know
			if (parse_edid(edid, iconv_descriptor,
				disp_name, sizeof(disp_name),
				disp_serial, sizeof(disp_serial),
				&serial)) {
				
				// if we are listing everything then print what we know
				if (list_all) {
					printf("%s \"%s\" \"%s\" %u\n", info->name,
							disp_name, disp_serial, serial);
					found = true;
				} else {
					// we only want to print if our search criteria are met
					if (((cond->name == NULL) ||
							(fnmatch(cond->name, disp_name, 0) == 0)) &&
						((cond->serial == NULL) ||
							(fnmatch(cond->serial, disp_serial, 0) == 0)) &&
						((cond->port == NULL) ||
						 	(strcmp(cond->port, info->name) == 0)) &&
						((!cond->serial_set) ||
							(cond->serial_number == serial))) {
						// it's a match so print the name of the port
						printf("%s\n", info->name);
						found = true;
					}
				}
			}

			XFree(edid);
		}
	}

	XRRFreeOutputInfo(info);

	return found;
}

int main(int argc, char *argv[]) {
	bool list_all = true;
	iconv_t iconv_descriptor;
	Display *dpy;
	Atom edid_atom;
	XRRScreenResources *screen;
	struct search_conditions cond = SEARCH_CONDITIONS_INIT;

	// parse command line options
	int opt;
	while ((opt = getopt(argc, argv, "ahvn:s:i:p:")) != -1) {
		switch (opt) {
			case 'a':
				list_all = true;
				break;
			case 'h':
				usage();
				return 0;
				break;
			case 'n':
				cond.name = optarg;
				list_all = false;
				break;
			case 'p':
				cond.port = optarg;
				list_all = false;
				break;
			case 's':
				cond.serial = optarg;
				list_all = false;
				break;
			case 'i':
				if (sscanf(optarg, "%" SCNu32, &cond.serial_number) != 1) {
					warnx("-i requires an integral argument");
					usage();
					return 0;
				} else {
					cond.serial_set = true;
				}
				list_all = false;
				break;
			case 'v':
				g_verbose = true;
				break;
			default:
				usage();
				return 1;
		}
	}

	// init a charset translator
	iconv_descriptor = init_iconv();

	// connect to the server and init some X stuff we will need
	if (! init_x(&dpy, &edid_atom, &screen)) {
		// nothing we can do so abort silently but non-zero
		return 2;
	}

	// loop through all the "outputs" in the "screen"
	for (int i = 0; i < screen->noutput; i++) {
		handle_single_output(dpy, screen, screen->outputs[i], edid_atom,
				iconv_descriptor,
				list_all, &cond);
	}
	
	XRRFreeScreenResources(screen);
	XCloseDisplay(dpy);

	return 0;
}
