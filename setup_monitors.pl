#!/usr/bin/perl

# the sort of script you could call at login to layout your monitors as you
# would like to have them. relies on find_monitor
# (https://gitlab.com/cqexbesd/find_monitor) and xrandr to do the actual work

use warnings;
use strict;

use Memoize;

# possible layouts of your monitors
#
# each entry (surrounded by []) is a list of monitors
# each monitor entry in a layout entry is surrounded by {}. there should be
# enough of {name, serial, port} to uniquely identify a monitor
# if you want the location set then you should also complete pos and rel.
# these arguments are just passed to xrandr so you can use anything xrandr
# accepts
my @layouts = (
	# home
	[
		{
			port => 'eDP-1',
		},
		{
			name => "IPS224",
			pos => "left-of",
			rel => "eDP-1"
		},
		{
			name => "DELL U2414H",
			serial => "9TG464AV63AL",
			pos => "right-of",
			rel => "eDP-1"
		}
	],
	# work (this doesn't work because my monitors both have the same serial
	# number...way to go Samsung...and the port they show up on is seemingly
	# random).
	[
		{
			port => 'eDP-1',
		},
		{
			name => "SMS27A850",
			serial => 'H1AK500000',
			pos => "left-of",
			rel => "eDP-1"
		},
		{
			name => "SMS27A850",
			serial => 'H1AK500000',
			pos => "right-of",
			rel => "eDP-1"
		},
	],
);

# use find_monitor to find the port a monitor is conencted to, or undef if not
# found
sub port_for_monitor {
	my($mon) = @_;

	# build the arguments we will pass to find_monitor
	my @args;

	if (exists $mon->{name}) {
		push(@args, '-n', $mon->{name});
	}

	if (exists $mon->{serial}) {
		push(@args, '-s', $mon->{serial});
	}

	if (exists $mon->{port}) {
		push(@args, '-p', $mon->{port});
	}

	# if there was nothing to search with then give up now
	if (@args == 0) {
		return undef;
	}

	# run find_monitor
	if (! open(FM, '-|', 'find_monitor', @args)) {
		warn("can't run find_monitor: $!\n");
		return undef;
	}

	# just assume the first result is the right one - if not then the user
	# will need to be more specific
	my $port = <FM>;
	chomp($port) if defined($port);

	close(FM);

	return $port;
}

# create a key for the cache of port_for_monitor results
sub normalize_port_for_monitor {
	my($mon) = @_;

	return join("\034", map { $mon->{$_} // '' } qw(name serial port));
}

# cache port_for_monitor results as we are likely to ask for things multiple
# times
memoize('port_for_monitor', NORMALIZER => 'normalize_port_for_monitor');

# go through the layouts and return the one with the most monitors matching
# what we can find attached
sub best_layout {
	my(@layouts) = @_;
	my $best;
	my $best_score = 0;

	foreach my $layout (@layouts) {
		my $this_score;
		foreach my $monitor (@{$layout}) {
			if (port_for_monitor($monitor)) {
				# we can see this one so the layout gets a point
				$this_score++;
			}
		}
		# check if this is the best one so far
		if ($this_score > $best_score) {
			$best_score = $this_score;
			$best = $layout;
		}
	}

	return $best;
}

# call xrandr to apply the given layout
sub apply_layout {
	my($layout) = @_;

	my @args;

	foreach my $mon (@{$layout}) {
		my $port;
		# not all monitors might be present and if they are they might not all
		# have configuration instructions
		if (exists($mon->{pos}) && exists($mon->{rel}) &&
			($port = port_for_monitor($mon))) {
			# this one needs configuring
			push(@args, '--output', $port, '--auto', '--' . $mon->{pos}, $mon->{rel});
		}
	}

	if (@args == 0) {
		return;
	}

	system('xrandr', @args);
}

# find the layout to apply
my $layout = best_layout(@layouts);
# and apply it
apply_layout($layout);
