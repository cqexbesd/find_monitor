PROG=find_monitor
SRCS=${PROG}.h ${PROG}.c edid.h edid.c
CSTD=gnu11
CFLAGS+=
LDADD+=-lX11 -lXrandr

.ifdef(DEBUG)
DEBUG_FLAGS+=-O0 -g
.endif

.ifdef(M32)
CFLAGS+=-m32
.endif

WARNS=9
NO_WERROR=true

NO_MAN=true

.include <bsd.prog.mk>
