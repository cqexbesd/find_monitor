A program to find your monitors
===============================

The Problem
-----------
I have several monitors connected to my laptop. I use `xrandr` to configure
the layout of my desktop. Unfortunately `xrandr` uses the names of the outputs
(i.e. the port names) to identify each monitor. For some reason there isn't a
constant mapping between the physical ports and their names though. That means
the command that correctly configres the layout I want this time might not
work next time.

The (Partial) Solution
----------------------
You can use this tool to find the name of the output that a particular monitor
is connected to. e.g.
```
>find_monitor -n IPS224
DP-1-2
```
With the aid of a shell script I can connect this with `xrandr` and have a
constant layout.

Building
--------
Builds using BSD make (e.g. `fmake` under Ubuntu). Requires the `libx11-dev`
and `libxrandr-dev` packages if you are using Ubuntu.

I have only tested under Ubuntu Linux as its the only place I run X11 at the
moment. Should be fairly portable I hope.
