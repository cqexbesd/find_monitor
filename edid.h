#ifndef EDID_H
#define EDID_H

#include <iconv.h>

// definitions from the EDID standard
#define EDID_SIZE 128
#define EDID_MAGIC 0x00FFFFFFFFFFFF00ULL
#define EDID_OFFSET_SERIAL 12
#define EDID_DESCRIPTOR_COUNT 4
#define EDID_OFFSET_DESCRIPTORS 54 // start of descriptor block
#define EDID_DESCRIPTOR_SIZE 18 // size of each descriptor
#define EDID_DESCRIPTOR_TYPE_SERIAL 0xFF
#define EDID_DESCRIPTOR_TYPE_NAME 0xFC
#define EDID_DESCRIPTOR_TEXT_SIZE 13 // length of text in a text descriptor
#define EDID_DESCRIPTOR_TEXT_OFFSET 5 // where text starts in a text descriptor

uint32_t unaligned_uint32(void *p);
uint64_t unaligned_uint64(void *p);
bool decode_string(iconv_t iconv_descriptor,
		char *input, size_t input_length,
		char *output, size_t output_size);
bool parse_edid(uint8_t *edid, iconv_t iconv_descriptor,
		char *disp_name, size_t disp_name_size,
		char *disp_serial, size_t disp_serial_size,
		uint32_t *serial);

#endif
