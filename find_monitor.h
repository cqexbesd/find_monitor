#ifndef FIND_MONITOR_H
#define FIND_MONITOR_H

#include <iconv.h>
#include <stdarg.h>
#include <stdbool.h>

#include <X11/Xlib.h>
#include <X11/extensions/Xrandr.h>

struct search_conditions {
	const char *name;
	const char *serial;
	const char *port;
	bool serial_set;
	uint32_t serial_number;
};

#define SEARCH_CONDITIONS_INIT { NULL, NULL, NULL, false, 0 }

void verbose(const char *fmt, ...);
iconv_t init_iconv(void);
bool init_x(Display **dpy, Atom *edid_atom, XRRScreenResources **screen);
bool edid_for_output(Display *dpy, RROutput output, Atom edid_atom, uint8_t **edid);
bool handle_single_output(Display *dpy, XRRScreenResources *screen,
        const RROutput output, const Atom edid_atom,
        iconv_t iconv_descriptor,
		const bool list_all, const struct search_conditions *cond);

#endif
